from setuptools import setup, find_packages

setup(
    name="sampleproject",
    version="0.0.1",
    packages=find_packages(exclude=["contrib", "docs", "tests"]),
    install_requires=[],
    extras_require={"dev": ["pylint", "black", "jedi", "pytest"]},
    python_requires=">3",
)
